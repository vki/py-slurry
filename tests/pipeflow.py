import unittest

from pyslurry.pipeflow import pipeflow
from pyslurry.pipeflow import models
from pyslurry import fluid


class HorizontalPipeTestCase(unittest.TestCase):

    def setUp(self):
        self.pipe = pipeflow.HorizontalPipe(D=1E-3, L=1, U=1)
        self.fluid = fluid.Water()

    def test_g(self):
        self.assertEqual(self.pipe.g, 9.81)

    def test_set_g(self):
        g = 10

        self.pipe.g = g
        self.assertEqual(self.pipe.g, g)

    def test_Re(self):
        Re = self.pipe.Re(self.fluid)
        self.assertAlmostEqual(Re, float(1000), delta=100)


class PoiseuilleTestCase(unittest.TestCase):
    def setUp(self):
        self.turbulent = pipeflow.HorizontalPipe(25E-3, 1, 2)
        self.laminar = pipeflow.HorizontalPipe(25E-3, 1, 0.002)
        self.fluid = fluid.Water()

        self.poiseuille = models.Poiseuille(self.fluid, self.laminar)

    def test_poiseuille_regime(self):
        self.assertEqual(self.poiseuille.regime, 'laminar')

    def test_poiseuille_pressure_drop(self):
        dP = 32*self.fluid.mu*self.laminar.U/self.laminar.D**2
        self.assertEqual(self.poiseuille.dp(), dP)

    def test_poiseuille_friction_factor(self):
        dP = 32*self.fluid.mu*self.laminar.U/self.laminar.D**2
        f = dP*2*self.laminar.D/self.fluid.rho/self.laminar.U**2
        self.assertEqual(self.poiseuille.f(), f)

    def test_poisueille_wrong_regime(self):
        with self.assertRaises(ValueError):
            self.poiseuille_turbulent = models.Poiseuille(self.fluid,
                                                          self.turbulent)

        self.assertRaises(ValueError)


class NonDevPoiseuilleTestCase(PoiseuilleTestCase):

    def test_non_developed_poiseuille_pressure_drop(self):
        raise NotImplemented


class TurbulentTestCase(unittest.TestCase):

    def setUp(self):
        self.turbulent = pipeflow.HorizontalPipe(25E-3, 1, 2)
        self.laminar = pipeflow.HorizontalPipe(25E-3, 1, 0.002)
        self.fluid = fluid.Water()

        self.turbulent_case = models.Turbulent(self.fluid, self.turbulent)

    def test_turbulent_regime(self):
        self.assertEqual(self.turbulent_case.regime, 'turbulent')

    def test_turbulent_friction_factor(self):
        f = 0.020933
        self.assertAlmostEqual(self.turbulent_case.f(), f, delta=1e-3)

    def test_turbulent_pressure_drop(self):
        f = 0.020933
        dP = f * self.fluid.rho/2 * self.turbulent.U**2/self.turbulent.D
        self.assertAlmostEqual(self.turbulent_case.dp(), dP, delta=10)
