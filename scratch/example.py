from pyslurry.fluid import SlurryWater
from pyslurry.fluid import SlushNitrogen
from pyslurry.pipeflow import HorizontalPipe
from pyslurry.pipeflow.models import DurandCondoliosGibert, Turbulent

slurry = SlurryWater(C=0.3, rho_s=2470, d=1E-3)
sln2 = SlushNitrogen(C=0.3, d=1E-3)

pipe = HorizontalPipe(D=55E-3, L=3, U=3)

slurry_case = DurandCondoliosGibert(slurry, pipe)
slush_case = DurandCondoliosGibert(sln2, pipe)

pure_water = Turbulent(slurry, pipe)
pure_ln2 = Turbulent(sln2, pipe)

print(slurry_case.dp(), slush_case.dp(), pure_water.dp(), pure_ln2.dp())
