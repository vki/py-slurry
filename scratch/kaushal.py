from pyslurry.pipeflow import HorizontalPipe
from pyslurry.fluid import Water
from pyslurry.pipeflow.models import DurandCondoliosGibert, Turbulent


water = Water()

Us = []
Dp = []
for i in range(5):
    pipe = HorizontalPipe(D=55E-3, L=3, U=i+2)
    kaushal = Turbulent(water, pipe)
    Dp.append(kaushal.dp())

print(Dp)



