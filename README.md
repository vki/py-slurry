pySlurry
========

pySlurry is a Python library that allows to compute easily important properties of multiphase flows (mainly slurry flows i.e. solid-liquid flows) using different models and correlations.

License
-------
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See `LICENSE` file.

Installation
------------

### Dependencies
The working package basically depends from `scipy` and `numpy` module that can be easily installed with `pip`. 

If you want to compile the documentation you will need `sphinx`

    pip install .[docs]

should install everything needed for docs. Moreover you will need to install also [pandoc](http://pandoc.org/installing.html#installing-from-source) that is used to convert the Markdown `README.md` to RestructuredText for sphinx documentation. 

If you want to run the interactive examples written with [jupyter](http://jupyter.org/) you will need obviosuly `jupyter`. The `[examples]` installation will provide all the necessary tools:

    pip install .[examples]



### VKI (Linux) Machines

The VKI machines are already shipped with a version of Python2 (2.7.5 at the time of writing this `README`) and Python3 (3.3.2). 
Unfortunately (or not) it's not possible to install directly a python module in the user home (i.e. using `pip` install --user`) since the `pip` utility is not available. 

The workaround is to create a virtual environment, with its own python executable and modules (in particular `pip`). To achieve this:

- Create a directory where to store the virtual environments:

    ```
    mkdir ~/.envs
    ```


- Create the virtual environment with Python3:

    ```
    virtualenv -p python3 ~/.virtualenvs/userpy3
    ```

- Activate the virtualenv

    ```
    source ~/.virtualenvs/userpy3/bin/activate
    ```

- Upgrade `pip` and `setuptools`:

    ```
    pip install --upgrade pip setuptools
    ```

- Install finally this package (run from inside the package root directory):
    
    ```
    pip install .
    ```

### Private Machine

On your private machine you could install it over the global system-wide installation, check before that numpy is already installed because in general its installation via `pip` is discouraged, then:

    pip install . 

should install the module.

It's however suggested to create a virtualenv, see the previous section to learn how. Once activated the virtualenv you can install the library in that isolated python installation without clugging the system-wide Python version.

Getting Started
--------------

Once you installed `pyslurry` you are ready to experiment with it: 

    >>> from pyslurry.fluid import Water
    >>> from pyslurry.pipeflow import HorizontalPipe
    >>> from pyslurry.pipeflow.models import Poiseuille
    >>> water = Water()
    >>> pipe = HorizontalPipe(D=15E-3, L=0.6, U=0.002)
    >>> case = Poiseuille(water, pipe)
    >>> print(case.Re)
    >>> 33.7078653933
    >>> print(case.dp())
    >>> 0.2531555555555

try to check the `examples/` that are interactive `jupyter` notebook that show the basics of the current features set. To run them, provided you installed with the `[examples]` install option, you have to run from the root of the package directory:

    jupyter notebook
