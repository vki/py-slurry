""" This module contains the classes for drag models """

from abc import ABCMeta, abstractmethod

import numpy


class DragModel(object):
    """ Abstract class for drag coefficient estimation """
    __metaclass__ = ABCMeta

    def __init__(self, slurry):
        self.slurry = slurry

    @abstractmethod
    def CD():
        """ This method returns the drag coefficient """
        pass


class Zanke1977(DragModel):
    """ This class implements the terminal velocity correlation from
    Zanke, 1977, retrieved from:

        *Sadat-Helbar, Amiri-Tokaldany*
        "Fall Velocity of Sediment Particles"
        Proceedings of the 4th IASME

    """

    def __init__(self, slurry, g=9.81):
        super().__init__(slurry)
        self.g = g

    def vt(self):
        nu = self.slurry.mu/self.slurry.rho
        d = self.slurry.d
        s = self.slurry.s
        g = self.g

        return 10*nu/d*((1+0.01*(s-1)*g*d**3/nu**2)**(0.5) - 1)

    def Rp(self):
        g = self.g
        s = self.slurry.s
        nu = self.slurry.mu/self.slurry.rho
        d = self.slurry.d

        Dgr = d*(g*(s-1)/nu**2)**(1/3)

        return 2.5*((16 + 0.16*Dgr**3)**(0.5) - 4)

    def CD(self):
        s = self.slurry.s
        g = self.g
        d = self.slurry.d
        vt = self.vt()

        return 4/3*(s-1)*g*d/vt**2


class TurianYuanDragCorrelation(DragModel):
    r""" This class implements the correlation for the drag coefficient reported
    in:

    **Turian and Yuan**
    "Flow of Slurries in Pipelines", *AlChE Journal (Vol. 23, No. 3), 1977,
    pag. 240*

    In that paper the pressure drop is given as difference from the friction
    factor of pure water (f-fw).

    .. math:: f-f_w = K C^{\alpha} f_w^{\beta} C_D^{\gamma} \left[
                \frac{v^2}{D g (s-1)} \right ]

    Note: It seems to not work correctly

    """

    def __init__(self, slurry, g=9.81):
        """ Constructor

        Args:
            slurry(pyslurry.fluid.Slurry): The slurry object
            g(float): Gravity field

        """
        super().__init__(slurry)
        self.g = g

    def CD(self):
        """ This method returns the drag coefficient for free falling spheres

        Returns:
            CD(float): The drag coefficient for free falling spheres
        """

        return (self.Lambda()/self._NRe())**2

    def Lambda(self):
        """ Lambda parameter to compute the CD

        Returns:
            Lambda(float): The lambda value

        """
        g = self.g
        d = self.slurry.d
        rho = self.slurry.rho
        rho_s = self.slurry.rho_s
        mu = self.slurry.mu

        return (4/3*g*(d**3)*rho*(rho_s - rho)/mu**2)**0.5

    def _NRe(self):
        """ This method implements the correlation described at pag. 240 for
        the Reynolds number of free-falling spheres

        Returns:
            NRe(float): Reynolds number for free falling spheres
        """

        log10A = numpy.log10(self.Lambda())
        log10NRe = -1.38 + 1.94*log10A - 8.60E-2*log10A**2 - 2.52E-2*log10A**3\
            + 9.19E-4*log10A**4 + 5.35E-4*log10A**5

        return 10**(log10NRe)
