#!/usr/bin/env python3
"""
This handy scripts computes the Durand-Condolios factor Phi
with different drag models and compare them with digitalize data.

Usage:
    durand_test -L <sand_number> -U <velocities>
    durand_test --rho=<particle_density> -C <concentration> -d <diameter> -U
        <velocities> -D <diameter>

Options:
    -L, --sand=<sand_number>             The sand identification number
                                            as in the Durand paper (L4/L8)

    --rho=<particle_density>             Density of solid particles
                                            [default: 2650]

    -C, --concentration=<concentration>  Concentration of the slurry

    -d, --particle_diameter=<diameter>   Diameter of the slurry particles

    -D, --pipe_diameter=<diameter>       Diameter of the pipe

    -U, --velocities=<velocities>        Comma-separated flow velocites

    -h, --help                           Print this help

Notes:
    If you chose to use the pre-setted L-sands the script automatically creates
    a plot against the related-digitalized data

"""

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os

from docopt import docopt
from pyslurry.pipeflow.models import DurandCondoliosGibert
from pyslurry.fluid import SlurryWater
from pyslurry.pipeflow import HorizontalPipe

plt.style.use('ggplot')
matplotlib.rcParams.update({'font.size': 16})


# Pre-configured sands

L4 = {
    'density': 2650,
    'diameter': 0.44E-3,
    'concentration': 0.5,
    'pipe_diameter': 0.150,
    'file_name': 'L4-D0p15'
}


L8 = {
    'density': 2650,
    'diameter': 2.04E-3,
    'concentration': 0.5,
    'pipe_diameter': 0.150,
    'file_name': 'L8-D0p15'
}

L_sands = {
    '4': L4,
    '8': L8
}


def read_file(filename):

    data = np.loadtxt(os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                   'image-data', filename))

    return data[:, 0], data[:, 1]


def compute(rho_s, C, d, Us, D, filename=None):
    """ Computes the slurry properties using the two drag models of Zanke and
    Turian

    Args:
        rho_s:          Solid particle density
        C:              Concentration
        d:              Particle diameter
        Us:             Array of flow velocities
        D:              Pipe Diameter

    Returns:
        Phi_Zanke:      The Durand parameters Phi computed using Zanke model
        Phi_Turian:     The Durand parameters Phi computed using Turian model

    """

    slurry = SlurryWater(C=C, rho_s=rho_s, d=d)

    Phi_Zanke = []
    Phi_Turian = []

    for U in Us:
        U = float(U)

        pipe = HorizontalPipe(U=U, D=D, L=1)
        case1 = DurandCondoliosGibert(slurry, pipe)
        case2 = DurandCondoliosGibert(slurry, pipe, drag="Zanke1977")

        Phi_Zanke.append(case2._Phi())
        Phi_Turian.append(case1._Phi())

    plt.plot(Us, Phi_Zanke, 's-', label="Zanke Drag Model")
    plt.plot(Us, Phi_Turian, 's-', label="Turian Drag Model")
    plt.title("Comparing Results with Experimental Data")
    plt.xlabel("Velocity [m/s]")
    plt.ylabel("Phi []")

    if filename:
        # Read from file
        U_file, Phi_file = read_file(filename)

        plt.plot(U_file, Phi_file, 'o', label=filename)

    plt.legend()

    return Phi_Zanke, Phi_Turian


if __name__ == '__main__':

    args = docopt(__doc__)

    sand_number = args['--sand']
    velocities = args['--velocities']

    if sand_number:
        rho_s = L_sands[sand_number]['density']
        C = L_sands[sand_number]['concentration']
        d = L_sands[sand_number]['diameter']
        D = L_sands[sand_number]['pipe_diameter']
        reference_file = L_sands[sand_number]['file_name']
        Us = velocities.split(',')

        Phi_Zanke, Phi_Turian = compute(rho_s, C, d, Us, D,
                                        filename=reference_file)

    else:
        rho_s = float(args['--rho'])
        C = float(args['--concentration'])
        d = float(args['--particle_diameter'])
        Us = list(map(float, velocities.split(',')))
        D = float(args['--pipe_diameter'])

        Phi_Zanke, Phi_Turian = compute(rho_s, C, d, Us, D)

    plt.show()


print('{:>9} {:>20} {:>20}'.format('Velocity:',
                                   'Phi (Zanke)',
                                   'Phi (Turian)'))

for i, _ in enumerate(Us):
    print('{:>8.3f} {:>20.3f} {:>20.3f}'.format(float(Us[i]), Phi_Zanke[i],
                                                Phi_Turian[i]))
