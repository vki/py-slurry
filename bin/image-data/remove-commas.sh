#!/usr/bin/env bash

FILES="$(ls .)"

for file in $FILES; do
	
	if [ "$file" != "remove-commas.sh" ] && [ "$file" != "reference-imgs" ]
	then
		echo $file
		sed -i 's/,/ /g' $file
	fi
done



